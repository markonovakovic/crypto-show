import React, { useEffect } from 'react';
/* import AppLayout from './components/layout/AppLayout'; */
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from './components/layout/Header';
import Footer from './components/layout/Footer';

import 'materialize-css/dist/css/materialize.min.css'; 
import M from 'materialize-css/dist/js/materialize.min.js'; 
import Index from './components/pages/Index';

/* COMPONENTS*/
import CryptoDetail from './components/pages/CryptoDetail';
import Contact from './components/pages/Contact';
import Register from './components/auth/Register';
import Login from './components/auth/Login';
import Dashboard from './components/dashboard';

/* CONTEXT STATE */
import CryptoState from './context/crypto/CryptoState';
import AuthState from './context/auth/AuthState';

/* PRIVATE ROUTE */
import PrivateRoute from './components/PrivateRoute';

import './App.scss';

//import InstallPompt from './components/InstallPrompt';


const App = () => {
  useEffect(() => {
    // Init Materialize JS
      M.AutoInit();
  });

    return (
      <Router>
      <AuthState>
      <CryptoState>
            {/*   <InstallPompt /> */}
              <Header />
              <div className="main">
                  <div className="container">
                    <Switch>
                        <Route path="/" exact component={Index} />
                        <Route path="/currencies/:id" exact component={CryptoDetail} />
                        <Route path="/contact" component={Contact} /> 
                        <Route path="/register" component={Register} /> 
                        <Route path="/dashboard" component={Dashboard} /> 
                        <Route component={Login} path="/login" exact />
                    </Switch>
                  </div>
              </div>  
              <Footer />    
        
  
        </CryptoState>
        </AuthState>
        </Router>
    )
}

export default App
