const  removeDuplicateDays = (arr) => {
    var newObj = {};
    if( arr !==  null ){
    return arr.filter(function (el) {
        var key = el.date
        var match = Boolean(newObj[key]);
        return (match ? false : newObj[key] = true);
    });
   }
}


const formatedDate  = (chartData) => {
  return  chartData && chartData.map( price => {  
        const dateFormated =  new Date(price[0]) 
        var year = dateFormated.getFullYear();
        var month = dateFormated.getMonth();
        var date = dateFormated.getDate();

        let result = {};
        let dt =  `${date} ${month} ${year}`;
       
        result = {...result, date:dt, price: price[1]}
    
        return result;
       
    });
}


export default {
    removeDuplicateDays,
    formatedDate
}