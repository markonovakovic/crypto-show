import * as firebase from "firebase/app";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyALAl8Ekvy33FdEXqU1P9APmLFtKswz9Oc",
    authDomain: "autentifikacija-54d71.firebaseapp.com",
    databaseURL: "https://autentifikacija-54d71.firebaseio.com",
    projectId: "autentifikacija-54d71",
    storageBucket: "autentifikacija-54d71.appspot.com",
    messagingSenderId: "535388443814",
    appId: "1:535388443814:web:ff26c195733a8a253d9067"
  };
  // Initialize Firebase
  const app  = firebase.initializeApp(firebaseConfig)

  export default app;
  /* 
  class Firebase {
       constructor(app) {
       // app.initializeApp(firebaseConfig)
        this.auth =  app.auth()
        this.db =  app.firestore();
       }

       login(email, password){
           return this.auth.signInWithEmailAndPassword(email, password)
                .catch(function(error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    if (errorCode === 'auth/wrong-password') {
                    alert('Wrong password.');
                    } else {
                    alert(errorMessage);
                    }
                    console.log(error);
                });
       }

       logout(){
           return this.auth.signOut()
       }

       async register(name, email, password){
          await this.auth.createUserWithEmailAndPassword(email, password);
          return this.auth.currentUser.updateProfile({
              dispalyName:name
          })
       }
  }


  export default new Firebase(app); */