import {
    GET_CURRENCIES,
    GET_COINS_MARKETS,
    SET_ALL_CRYPTOCURRENCY,
    GET_CRYPTOCURRENT_BY_NAME,
    SEARCH_CRYPTOCURRENCY,
    CLEAR_SEARCH,
    SET_CRYPTO_PER_PAGE,
    UPDATE_QUERY,
    SORT_BY_NAME_ASC,
    SORT_BY_NAME_DESC,
    GET_MARKET_CHART,
    SET_LOADING,
    FILTER_CHART_BY_MODEL,
    FILTER_CHART_BY_DAYS,  // eslint-disable-line'
    TOGGLE_INSTALL_PROMPT
} from '../types';



export default (state, action) => {
    switch (action.type) {
      case GET_CURRENCIES:
        return {
          ...state,
          cryptocurrencies: action.payload,
          loading: false
        };
    case GET_COINS_MARKETS:
        return {
            ...state,
            markets: action.payload,
            loading: false
        };
    case GET_CRYPTOCURRENT_BY_NAME:
        return {
          ...state,
          cryptoCurrency:action.payload,
          loading: false
        }
    case GET_MARKET_CHART:

      return {
        ...state,
        marketChartData: action.payload,
        loading:false
      }
    case FILTER_CHART_BY_MODEL:

      return {
        ...state, 
        chartQuery: { ...state.chartQuery, model : action.payload}
      }
    case FILTER_CHART_BY_DAYS:
      return {
        ...state,
        chartQuery: { ...state.chartQuery, days: action.payload },
          
      }
    case SET_CRYPTO_PER_PAGE: 
       return {
         ...state,
         basicQuery:{...state.basicQuery, per_page:action.payload } 
       }
     case UPDATE_QUERY:
      return {
        ...state,
        basicQuery: {...state.basicQuery, [action.payload.property] : action.payload.value},
        loading: false
      }
      case SORT_BY_NAME_ASC:
        console.log('asc red');
          return {
            ...state,
            markets: state.markets.sort((a, b) => (a.id > b.id) ? 1 : -1)
      }
      case SORT_BY_NAME_DESC:
        console.log('des red');
          return {
            ...state,
            markets:state.markets.sort((a, b) => (a.id < b.id) ? 1 : -1) 
          }
      case SET_ALL_CRYPTOCURRENCY:
        return {
          ...state,
          basicQuery:{...state.basicQuery, per_page:1000},
          loading: false
        }
      case SEARCH_CRYPTOCURRENCY:

          return {
            ...state,
            filteredmarkets: state.markets.filter(market =>{
                const regex = new RegExp(action.payload,'gi');
                return market.id.match(regex);
            }),
            loading: false
          }
      case CLEAR_SEARCH: 
          return {
            ...state,
            filteredmarkets:null,
            basicQuery: { ...state.basicQuery},
            loading:false
          }
      case SET_LOADING:
        return {
          ...state,
          loading: true
        }
      case TOGGLE_INSTALL_PROMPT:
        return {
          ...state,
          hideInstallPrompt:action.payload
        }
 
      default:
        return state;
    }
  };
  