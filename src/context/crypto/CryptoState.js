import React, { useReducer, useEffect } from 'react';
import axios from 'axios';
import CryptoContext from './cryptoContext';
import CryptoReducer from './cryptoReducer';

import {
    GET_CURRENCIES,
    GET_COINS_MARKETS,
    SET_ALL_CRYPTOCURRENCY,
    SET_CRYPTO_PER_PAGE,
    UPDATE_QUERY,
    SORT_BY_NAME_ASC,
    SORT_BY_NAME_DESC,
    SEARCH_CRYPTOCURRENCY,
    CLEAR_SEARCH,
    GET_CRYPTOCURRENT_BY_NAME,
    GET_MARKET_CHART,
    FILTER_CHART_BY_MODEL,
    FILTER_CHART_BY_DAYS,
    SET_LOADING,
    TOGGLE_INSTALL_PROMPT
 
} from '../types';

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

const initQuery = {
    currency: 'usd',
    order: 'market_cap_desc',
    per_page: 50,
    page: 1,
    sparkline: false
}

const queryString = localStorage.getItem('query');
const queryData = isEmpty(JSON.parse(queryString)) ? initQuery : JSON.parse(queryString);

const marketsString = localStorage.getItem('markets');
const marketsStore = JSON.parse(marketsString) ? JSON.parse(marketsString) : [];

const CryptoState = props =>{
    const initialState = {
        markets: marketsStore, //null,
        filteredmarkets:null,
        cryptoCurrency: null,
        loading:false,
        basicQuery: queryData,
        marketChartData:null,
        chartQuery: {
            model:'total_volumes',
            days:'30'
        },
        hideInstallPrompt:false
       
    };

    const [state, dispatch] = useReducer(CryptoReducer, initialState);

   useEffect(() =>{
       localStorage.setItem('query', JSON.stringify(state.basicQuery));
       localStorage.setItem('markets', JSON.stringify(state.markets));
   });

   const getCryptoList = async () => {
     
        try {
            const res  = await  axios.get('https://api.coingecko.com/api/v3/coins/list');
            const data = await res.data;
            dispatch({
                type:GET_CURRENCIES,
                payload: data
            });
            
        } catch (error) {
           console.log(error);
        }
    

    }
    

    const getCoinsMarkets = async () => {
        const {currency, order, per_page, page, sparkline } = state.basicQuery;
        try {
             const res = await axios.get(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=${currency}&order=${order}&per_page=${per_page}&page=${page}&sparkline=${sparkline}`);
             const data =  res.data;
             dispatch({
                 type:GET_COINS_MARKETS,
                 payload:data
             })

        } catch (error) {
            console.log(error);
        }
    }

    const getCryptoCurrencByName = async(name) => {
       
        setLoading();
        const res = await axios.get(`https://api.coingecko.com/api/v3/coins/${name}?localization=false&tickers=true`);
        const data =   await res.data;

        dispatch({
            type:GET_CRYPTOCURRENT_BY_NAME,
            payload:data
        })
        
    }

    const cryptoMarketChart = async (name, days) => {

      //  const { days } = state.chartQuery;
        const { currency } = state.basicQuery;
      
      //  setLoading()
        const res = await axios.get(`https://api.coingecko.com/api/v3/coins/${name}/market_chart?vs_currency=${currency}&days=${days}`);
        const data = await res.data;
  
        dispatch({
            type:GET_MARKET_CHART,
            payload:data
        })
    }

    const filteredChartByModel = (data) => {

        dispatch({
            type: FILTER_CHART_BY_MODEL,
            payload:data
        })
    }


    const filteredChartByDays = (data) => {

        dispatch({
            type: FILTER_CHART_BY_DAYS,
            payload: data
        })
    }


    const updatePerPage = (updatedValue) =>{
        dispatch({
            type:SET_CRYPTO_PER_PAGE,
            payload:updatedValue
        });
    }
    
    const updateQuery = (updatedValue) =>{
        setLoading();
        dispatch({
            type:UPDATE_QUERY,
            payload: updatedValue
        });
    }

    const sortByNameAsc = () => {
        dispatch({
          type:SORT_BY_NAME_ASC,
        })
    }
    const sortByNameDesc = () => {
        dispatch({
          type:SORT_BY_NAME_DESC,
        })
    }

    const getAllCryptocurreny = async() =>{
        setLoading();
        dispatch({
            type:SET_ALL_CRYPTOCURRENCY
        })
    }

    const searchCryptoCurrency = async(text) => {
        dispatch({
            type: SEARCH_CRYPTOCURRENCY,
            payload:text
        })
    }

    const clearSearchCrypto =  () => {
        dispatch({
            type: CLEAR_SEARCH,
          
        })
    }
    const clearFilteredCryptoCurrency = async(text) => {
        dispatch({
            type: CLEAR_SEARCH,
            payload:text
        })
    }

    const toggleInstallPrompt = (status) => {

        dispatch({
            type:TOGGLE_INSTALL_PROMPT,
            payload:status
        })
    }

        
    //Set Loading
const setLoading = () => dispatch({ type:SET_LOADING });
    return ( <CryptoContext.Provider
    
     value={{
        basicQuery:state.basicQuery,
         cryptocurrencies:state.cryptocurrencies,
         markets:state.markets,
         filteredmarkets: state.filteredmarkets,
         cryptoCurrency: state.cryptoCurrency,
         marketChartData:state.marketChartData,
         chartQuery: state.chartQuery,
         loading:state.loading,
         hideInstallPrompt: state.hideInstallPrompt,
         getCryptoList,
         getCoinsMarkets,
         getCryptoCurrencByName,
         updatePerPage,
         updateQuery,
         sortByNameAsc,
         sortByNameDesc,
         getAllCryptocurreny,
         searchCryptoCurrency,
         clearSearchCrypto,
         clearFilteredCryptoCurrency,
         cryptoMarketChart,
         filteredChartByModel,
         filteredChartByDays,
         toggleInstallPrompt
       
         
     
     }}
    >
        {props.children}

    </CryptoContext.Provider>
    );
};


export default CryptoState;