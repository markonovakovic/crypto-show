import React, { useState, useReducer, useEffect } from 'react';
import { withRouter } from "react-router-dom";
import axios from 'axios';
import AuthContext from './authContext';
import AuthReducer from './authReducer';
import app from '../../services/firebase-auth';

import {
    REGISTER_NEW_USER,
    LOGIN
} from '../types';

const auth =  app.auth()
const db =  app.firestore();

export const AuthProvider = ({ children, history }) => {

    const initialState = {
        user:null,
        newUser:null
    };
    const [state, dispatch] = useReducer(AuthReducer, initialState);
    const [currentUser, setCurrentUser] = useState(null);
    

    useEffect(() => {
       
      app.auth().onAuthStateChanged((user) =>{
          setCurrentUser(user);
      });
      if(currentUser){
        console.log(auth.currentUser);
        }
    }, []);
  
   const login = async (email, password) => {
    console.log(email, password);
    try {
       
        const result = await auth.signInWithEmailAndPassword(email, password);
       
        dispatch({
            type:LOGIN,
            payload:result
        })
        history.push('/dashboard');
    } catch (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === 'auth/wrong-password') {
        alert('Wrong password.');
        } else {
        alert(errorMessage);
        }
        console.log(error);
    }
       
    }
    const  register = async (name, email, password) =>{
        try {
            await auth.createUserWithEmailAndPassword(email, password);
           const newUser =  auth.currentUser.updateProfile({
                displayName:name
            });
          
           console.log(currentUser);
             dispatch({
                type:REGISTER_NEW_USER,
                payload:newUser
            }); 
            history.push('/');
        } catch (error) {
            console.log(error.message);
        }
       
     }

    const logout = async () =>{
         await auth.signOut()
         history.push('/');
    }

    return (
      <AuthContext.Provider
        value={{
          newUser:state.newUser,
          currentUser:currentUser,
          register,
          login,
          logout
        }}
      >
        {children}
      </AuthContext.Provider>
    );
  };


  export default withRouter(AuthProvider)