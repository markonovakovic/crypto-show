import {
    REGISTER_NEW_USER,
    LOGIN
} from '../types'

export default (state, action) => {
    switch(action.type){
        case REGISTER_NEW_USER:
          
            return {
                ...state,
                newUser: action.payload.providerData[0]
            } 
        case LOGIN:
               
                return {
                    ...state,
                    user:action.payload
                 }
        default:
            return state;
        
    }
}