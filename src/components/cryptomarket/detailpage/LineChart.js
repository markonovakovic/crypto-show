import React, { useState, useEffect } from 'react'
import Chart from 'react-apexcharts'
import './Chart.scss';
import chartUtils from '../../../utils/chart-utils';

const LineChart = ({chartData}) => {

    const formatedDate =  chartUtils.formatedDate(chartData);
    const newObj =  chartUtils.removeDuplicateDays(formatedDate);

    const [series] = useState([
        {
            name: "series-1",
            data: chartData && newObj.map( item => item.price.toFixed(4))
        }
    ]);

    // eslint-disable-next-line
    const [options, setOptions] = useState({
        chart: {
            type: 'line'
          },
        xaxis: {
            categories: chartData && newObj.map( item => item.date),
            labels: {
                style: {
                    colors: [],
                    fontSize: '12px',
                    fontFamily: 'Helvetica, Arial, sans-serif',
                    cssClass: 'apexcharts-xaxis-label',
                },
            }
        },
        responsive: [
            {
                breakpoint: 680,
                options: {
                    chart: {
                        width: "100%"
                        
                    },
                    xaxis: {
                        labels: {
                            style: {
                                colors: [],
                                fontSize: '6px',
                                fontFamily: 'Helvetica, Arial, sans-serif',
                                cssClass: 'apexcharts-xaxis-label',
                            },
                        }
                    },
                    legend: {
                        show: true
                    }
                }
            }
        ]

    })

   
    return (
        <Chart
            options={options}
            series={series}
            type="line"
            width="800"
        />
    )
}



export default LineChart
