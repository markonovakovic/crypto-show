import React , { useEffect, useContext }from 'react';
import CryptoContext from '../../../context/crypto/cryptoContext';

const ChartFilter = () => {

    const { filteredChartByModel, filteredChartByDays } = useContext(CryptoContext);

 //   const [ query, setQuery ] =  useState({});
 

    useEffect(() => {
       
     
      
    });
    const handleClick = (e) => {
     
        if (e.target.name === 'days'){
          //  setQuery({ ...query,  days:e.target.value});
            filteredChartByDays(e.target.value);
            
        }else {
        // setQuery({...query, model:e.target.value});
            filteredChartByModel(e.target.value);

          
        }
     
      
   
 //       setFilterChart(data);
    }

 
     return (
        <div>
            <div className="row">
                <div className="col m5">
                     <button className="filter-chart-btn" name="model" value='prices' onClick={handleClick} >Price</button>
                     <button className="filter-chart-btn" name="model" value='market_caps' onClick={handleClick} >Market</button>
                     <button className="filter-chart-btn" name="model" value='total_volumes' onClick={handleClick} >Tradein</button> 
                </div>
                 <div className="col m7 right-align">
                     <button className="filter-chart-btn" name="days" value='1' onClick={handleClick}>24h</button>
                     <button className="filter-chart-btn" name="days" value='7'  onClick={handleClick}>7d</button>
                     <button className="filter-chart-btn" name="days" value='14' onClick={handleClick}>14d</button>
                     <button className="filter-chart-btn" name="days" value='30' onClick={handleClick}>30d</button>
                 
                </div>
            </div>          
        </div>
    )
}


export default ChartFilter
