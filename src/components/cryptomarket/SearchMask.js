import React from 'react'

const SearchMask = ({showAllCryptoCurrency, search}) => {

  
    return (
        <div className="row" style={{marginBottom:'0px'}}>
        <div className="col s12">
            <div className="input-field col s12">
              <input type="text" id="autocomplete-input" className="autocomplete" onClick={showAllCryptoCurrency} onChange={search} />
              <label htmlFor="autocomplete-input" >Search Crypto</label>
            </div>
        </div>
        
      </div>
    )
}

export default SearchMask
