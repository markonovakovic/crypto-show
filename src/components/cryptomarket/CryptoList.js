import React, { useState, useEffect, useContext } from 'react'
import MaterializeInit from '../layout/MaterializeInit';
import CryptoContext from '../../context/crypto/cryptoContext';
import CryptoItem from './CryptoItem';
import FilterMask from './FilterMask';
import SearchMask from './SearchMask';

import arrowUp from '../../static/images/chevron-arrow-up.svg';
import arrowDown from '../../static/images/chevron-arrow-down.svg';
const CryptoList = () => {

  const [ text, setText ] = useState('');
  const [ isSortAsc, setIsSortAsc ] = useState(true);

  const cryptoContext = useContext(CryptoContext);

  const { markets, filteredmarkets, searchCryptoCurrency, clearSearchCrypto, basicQuery: { per_page, page }, sortByNameAsc, sortByNameDesc, getAllCryptocurreny, getCoinsMarkets } = cryptoContext;
  useEffect(() =>{
    getCoinsMarkets();

    if (text === "" && filteredmarkets !== null ){
      console.log('radi');
        clearSearchCrypto()
    }
  
    // eslint-disable-next-line
  }, [filteredmarkets, text])
     
    const sortByName = () =>{
   
    if(isSortAsc === true){
      setIsSortAsc(false);
      sortByNameAsc()
      
    }else{
      setIsSortAsc(true);
      sortByNameDesc()
    }

  }

  const showAllCryptoCurrency = () =>{
    getAllCryptocurreny();
  }


  const search = (e) =>{
   
    setText(e.target.value);
    if(e.target.value !==""){
      console.log(text);
      searchCryptoCurrency(e.target.value);
    }else{
      clearSearchCrypto()
    }
  }

  const marketList = filteredmarkets ? filteredmarkets : markets;  
  let sortNameArrow =  isSortAsc ? arrowUp : arrowDown;

    return (
        <MaterializeInit>
          <div className="headline" style={{marginBottom:'60px'}}>
              <h4 className="center-align">Top {per_page} Cryptocurrencies by Market Capitalization { page > 1 ? `(Page ${page})` : '' }</h4>
          </div>
          <div className="row" style={{marginBottom:'0px'}}>
               <div className="col s12 m4">
               <SearchMask showAllCryptoCurrency={showAllCryptoCurrency} search={search} /> 
               </div>
               <div className="col s12 m8">
               <FilterMask />
               </div>
          </div>
          <table>
            <thead>
              <tr>
                  <th>#</th>
                  <th onClick={sortByName}>Name{'  '} <img src={sortNameArrow} style={{marginLeft:'25px'}} alt="arrow"/></th>
                  <th>Price</th>
                  <th>Change (24)</th>
              </tr>
            </thead>

            <tbody>
            
              {  marketList &&  marketList.map(market => <CryptoItem  crypto={market} key={market.id} /> ) }
            
            </tbody>
          </table>
        </MaterializeInit>
    )
}

export default CryptoList
