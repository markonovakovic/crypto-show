import React, { useState, useEffect, useContext } from 'react';
import CryptoContext from '../../context/crypto/cryptoContext';

const FilterMask = () => {
    const  {getCoinsMarkets, updateQuery , basicQuery} = useContext(CryptoContext);
  
      // eslint-disable-next-line
    const [ query, setQuery ] = useState(
        {
            currency:'',
            order:'',
            per_page:'50',
            page:null,
            sparkline:false
        }
    );
    useEffect(() =>{
         
        getCoinsMarkets();
       // eslint-disable-next-line
    },[query, basicQuery]);
    

    const handleChanges = (e) =>{
        setQuery({...query, [e.target.name]: e.target.value});
        updateQuery({property: e.target.name, value: e.target.value} );
           
    }

    const changePage = (step) =>{
       

        if(step === 'next'){
             console.log('sledeci')
             updateQuery({property:'page', value: basicQuery.page + 1} );
        }else {
            updateQuery({property:'page', value: basicQuery.page - 1} );
        }
    }

    const currencyList = [
        'usd', 'eur','jpy'
    ];

    const currencyOption = currencyList.map(item => {
        // eslint-disable-next-line
        return <option value={item} key={item}>{item.toUpperCase()}</option>
    });
    return (
        <div className="row right" style={{marginBottom:'0px', border:'1px solid #ccc',marginLeft:'20px'}}>
            <div className="input-field col s5 m2">
                <select onChange={handleChanges} name='currency' value={basicQuery.currency}>
                 
                     {currencyOption} 
                </select>
                    <label>Currency</label>
            </div>
            <div className="input-field col s5 m3">
                <select onChange={handleChanges} name='per_page' value={basicQuery.per_page}>
                  <option  disabled value="">{basicQuery.per_page}</option> 
                    <option value="50">50</option>   
                    <option value="100">100</option> 
                    <option value="150">150</option> 
                    <option value="200">200</option> 
                    <option value="250">250</option> 
                    <option value="1000">All</option> 
                </select>
                <label>Display Per Page</label>
            </div>
            <div className="col s12 m7">
                <div className="btns-section" style={{paddingTop:"25px", display:'inline-block'}}>
    {  basicQuery.page !== 1  ? <span className="waves-effect waves-light  btn-small left " onClick={() => changePage('prev')}> <i className="material-icons left" style={{fontSize:'10px'}} >&laquo; Prev {query.per_page}</i></span> : null } 
                     <span className="waves-effect waves-light  btn-small right " onClick={() => changePage('next')} style={{marginLeft:'10px'}}><i className="material-icons left" style={{fontSize:'10px'}}>Next {query.per_page}</i> &raquo;</span>
                </div>
           </div>
        </div>
    )
}

export default FilterMask
