import React from 'react';
import { Link } from 'react-router-dom';

const CryptoItem = ({crypto}) => {
    const { 
        market_cap_rank,
        image,
        id,
        current_price,
        price_change_24h

    } =  crypto;
    return (
    <tr >
        <td>{market_cap_rank}</td>
        <td >
                <Link to={`/currencies/${id}`} style={{display:'flex',alignItems:'center'}} >
               <img  style={{width:'30px'}} className="crypto-image" src={image} alt={id} />
               <h6 style={{marginLeft:'10px', fontWeight:'bold'}}>{id}</h6>
            </Link>
        </td>

        <td>{current_price}</td>

        <td>{price_change_24h}</td>
    </tr>
   
    )
}

export default CryptoItem
