import React, { Fragment, useState, useContext, useEffect } from 'react'
import { Link } from 'react-router-dom';
import AuthContext from '../../context/auth/authContext'
import logo from '../../static/images/coinmarketcap_grey_1.svg';
import MaterializInit from './MaterializeInit';
import M from 'materialize-css/dist/js/materialize.min.js'; 

const Header = () => {
  const [ sidenav, setSidenav ] =  useState(null);
  useEffect(() =>{
    var elem = document.querySelector('.sidenav');
    setSidenav(elem);

  },[]);
   const closeNavbar = () => {
  //  var instance = M.Sidenav.getInstance(elem);
      const instance = M.Sidenav.getInstance(sidenav)
      instance.close();
 
   }
    return (
      <MaterializInit>
      <nav className="nav-extended">
          <div className="nav-wrapper">
            <Link to="/" className="brand-logo">  <img style={{padding:'10px', marginLeft:'20px'}} src={logo} className="App-logo" alt="logo" /></Link>
            <div  data-target="mobile-demo" className="sidenav-trigger" style={{marginTop:'8px'}}><div className="container" style={{zIndex:'1000'}}>
              <div className="bar1"></div>
              <div className="bar2"></div>
              <div className="bar3"></div>
            </div></div>

            <ul id="nav-mobile" className="right hide-on-med-and-down">
             <Navbar />
            </ul>
          </div>
        </nav>
      
          <ul className="sidenav" id="mobile-demo" style={{maxWidth:'250px'}}>
            <div onClick={closeNavbar} style={{padding:'10px', border:'1px solid #ccc'}}>X</div>
              <Navbar closeNavbar={closeNavbar} />
          </ul>
      </MaterializInit>
    )
};


const Navbar = props => {
  const { logout, currentUser } = useContext(AuthContext);

  useEffect(() =>{
    if(currentUser){
     
  
    }
    
  })
  return (
    <Fragment>
          { currentUser &&   <li><Link to="/dashboard" onClick={props.closeNavbar}>Dashboard</Link></li> }
          <li><Link to="/">Home</Link></li>
          <li><Link to="/contact">Contact</Link></li> 
        
         {
           currentUser ? 
               ( <li onClick={() =>logout() }><a>Logout</a></li>):
               (
                 <Fragment>
                  <li><Link to="/register">Register</Link></li> 
                  <li><Link to="/login">Login</Link></li> 
                </Fragment>
               )
         }
    </Fragment>
  )
}

export default Header
