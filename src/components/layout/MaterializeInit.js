import React, { Fragment, useEffect } from 'react';
import M from 'materialize-css/dist/js/materialize.min.js'; 


const MaterializeInit = (props) => {

    useEffect(() =>{
        M.AutoInit();
     })
    return (

    <Fragment>
        
            {props.children}
                  
     </Fragment>
    
    )
}

export default MaterializeInit
