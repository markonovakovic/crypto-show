import React from 'react'

const Footer = () => {
    return (
        <footer className="page-footer">
        <div className="container">
          <div className="row">
            <div className="col l6 s12">
              <h5 className="dark-text">Footer Content</h5>
              <p className="dark-text text-lighten-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, </p>
            </div>
            <div className="col l4 offset-l2 s12">
              <h5 className="dark-text">Links</h5>
              <ul>
                <li><a className="grey-text" href="#!">Link 1</a></li>
                <li><a className="grey-text" href="#!">Link 2</a></li>
                <li><a className="grey-text" href="#!">Link 3</a></li>
                <li><a className="grey-text" href="#!">Link 4</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div className="footer-copyright blue-grey darken-4">
          <div className="container">
             © 2019 Copyright Bore Spuzic Kvaka
          <a className="grey-text right" href="#!">More Links</a>
          </div>
        </div>
      </footer>
    )
}

export default Footer