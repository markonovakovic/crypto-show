import React, { useEffect, useState, useContext } from 'react'
import CryptoContext from '../context/crypto/cryptoContext';



const InstallPompt = props => {
     
    const [deferredPrompt, setDeferredPrompt] = useState(null);

    const { toggleInstallPrompt, hideInstallPrompt  } = useContext(CryptoContext);
    window.addEventListener('beforeinstallprompt', (e) => {
        // Prevent Chrome 67 and earlier from automatically showing the prompt
        e.preventDefault();

        // Stash the event so it can be triggered later.
        setDeferredPrompt(e);


    });
    useEffect(() => {
       console.log(deferredPrompt);

      
    }, [deferredPrompt]);

    const addOnScreen = () => {
        console.log(deferredPrompt);
      //  deferredPrompt.prompt();
        setDeferredPrompt(null);
        toggleInstallPrompt(true);
        console.log(hideInstallPrompt);
    }
   let output =    !hideInstallPrompt ?  (
        <div>
          {/*   <a className="waves-effect waves-light btn modal-trigger" href="#modal1">Modal</a> */}
            <div>
                <div className="modal-content">
                    <h4>Add on your mobile</h4>
                    <p>A bunch of text</p>
                </div>
                <div className="modal-footer">
                    <button onClick={addOnScreen}>Add on screen</button>
                </div>
            </div>
        </div>
    ) : null;
  
    return output;
}



export default InstallPompt
