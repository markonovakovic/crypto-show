import React, { useState, useEffect, useContext } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import firebase from '../../services/firebase-auth';
import AuthContext from '../../context/auth/authContext'

import './auth.scss';
const Login = ({history}) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { login, currentUser } = useContext(AuthContext);

    useEffect(() =>{
     
       if(currentUser){
        console.log(currentUser);
          // history.goBack();
       }
    },[])

    const onLogin = (e) =>{
        e.preventDefault();
        console.log(email, password, 'from login');
        login(email, password);
        setEmail('');
        setPassword('');
    }

   if(!currentUser){
        return (
            <div className="login-register-section">
                <form  onSubmit={onLogin}>
                    <div className="row">
                        <div className="input-field col s12">
                            <input id="email_inline" name={email} type="email" className="validate" onChange={e => setEmail(e.target.value)} />
                            <label htmlFor="email_inline">Email</label>
                            <span className="helper-text" data-error="wrong" data-success="right"></span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                        <input id="password" name={password} type="password" className="validate" onChange={e => setPassword(e.target.value)} />
                        <label htmlFor="password">Password</label>
                        </div>
                    </div>
                    <button className="waves-effect waves-light btn right" type="submit">Login</button>
                </form>
            </div>
        
        )

    } else {
        return <Redirect to="/" /> 
    }
   
}

export default withRouter(Login)
