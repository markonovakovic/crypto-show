import React, { useState, useContext } from 'react'
import { withRouter } from 'react-router-dom';
//import firebase from '../../services/firebase-auth';
import './auth.scss';
import AuthContext from '../../context/auth/authContext'
const Register = (props) => {
    const [name, setName ] =  useState('');   //eslint-disable-line
    const [email, setEmail ] =  useState(''); 
    const [password, setPassword ] =  useState(''); 
    const [password2, setPassword2 ] =  useState(''); 
   // const [quote, setQuote ] =  useState(''); 
    const { register } = useContext(AuthContext);
    const onSubmit = async (e) =>{
          e.preventDefault();
          console.log('submit');
          try {
              register(name, email, password);
            //   await firebase.register(name, email, password);
             //  props.history.replace('/');
          } catch (error) {
               alert(error.message);
          }
    }
  

    return (
        <div className="login-register-section">
           
                <form className="" onSubmit={onSubmit}>
                <div className="row">
                    <div className="input-field col s12">
                    <input  id="first_name" type="text" className="validate" name={name} onChange={e => setName(e.target.value)} />
                    <label htmlFor="first_name">Name</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <input id="email_inline"className="validate"  type="email"name={email} onChange={e => setEmail(e.target.value)}/>
                        <label htmlFor="email_inline">Email</label>
                        <span className="helper-text" data-error="wrong" data-success="right"></span>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="password" type="password" name={password} className="validate" onChange={e => setPassword(e.target.value)} />
                    <label htmlFor="password">Password</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="password2" type="password" name={password2} className="validate" onChange={e => setPassword2(e.target.value)} />
                    <label htmlFor="password2">Repeat Password</label>
                    </div>
                </div>
                
                 <button className="waves-effect waves-light btn right" type="submit">Register</button>
                 </form>
          
                    
        </div>
    )
}

export default withRouter(Register)
