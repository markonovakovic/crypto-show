import React, { useEffect, useContext } from 'react'
import PropTypes from 'prop-types'
import MaterializeInit from '../layout/MaterializeInit';
import CryptoContext from '../../context/crypto/cryptoContext';
import Preloader from '../UI/Preloader';
import LineChart from '../cryptomarket/detailpage/LineChart';
import ChartFilter from '../cryptomarket/detailpage/ChartFilter';
import { htmlDecode }  from '../../utils/html-decode-utils';

const CryptoDetail = props => {

    const { cryptoCurrency, basicQuery:{currency}, chartQuery, loading, getCryptoCurrencByName, cryptoMarketChart, marketChartData } = useContext(CryptoContext);

   
    useEffect(() => {
    console.log('test');
        getCryptoCurrencByName(props.match.params.id);
    
        // eslint-disable-next-line
    }, [props, chartQuery]);

    useEffect(() => {
  
        cryptoMarketChart(props.match.params.id, chartQuery.days);



        // eslint-disable-next-line
    }, [chartQuery]);
   
    
  const urlString = (url) =>{
     return url.replace(/(^\w+:|^)\/\//, '').split('/')[0];
  }
    if(cryptoCurrency && loading === false){
        const {
            name,
            symbol,
            image,
            description,
            market_cap_rank,
            links: {
                homepage,
                blockchain_site,
            },
            market_data: {
                  current_price,
                  price_change_percentage_1h_in_currency,
                  market_cap,
                  circulating_supply,
                  total_supply,
                  total_volume
                }
         } = cryptoCurrency;
    

        const blokchainSites = blockchain_site.map((site, key) => {
             // eslint-disable-next-line
            if (site && key < 3) return <a href={site} className="collection-item" key={key} target="_blank" rel="noopener noreferrer"><span className="badge blue-text blue lighten-5" style={{ marginLeft: 0, marginRight:'7px', padding: '0 6px 0 0' }}>{urlString(site)}</span></a> ;
            return null;
        })
       
        let filteredChartData = marketChartData && marketChartData;

        if (chartQuery.model ==='market_caps'){
            filteredChartData = marketChartData &&  marketChartData.market_caps
        } else if (chartQuery.model === 'total_volumes') {
            filteredChartData = marketChartData &&  marketChartData.total_volumes
        }else {
            filteredChartData = marketChartData && marketChartData.prices
        }

        return (
            <MaterializeInit className="row">
                <div className="col s12 m12">
                    <div className="card">
                        <div className="card-content">
                            <div className="headline">
                                <div className="row">
                                    <div className="col s12 m12 l4" style={{display:'flex', alignItems:'center'}}>
                                        <img src={image.thumb} style={{width:'50px', height:'50px', marginRight:'20px'}} alt={name}/>  
                                        {' '}<h3>{name}</h3> <h5 style={{marginLeft:'10px', marginTop:'20px'}}>{symbol}</h5>
                                    </div>
                                    <div className="col s12 m8 l8 flex aling--center" style={{marginTop:'16px'}}>
                                        <h4>{current_price[currency]}{' '} {currency}</h4>{' '} <p className="grey-text" style={{marginLeft:'10px', marginTop:'10px'}}> ( {price_change_percentage_1h_in_currency[currency]}% )</p>
                                    </div>
                                </div>
                                <div className="row currency-info">
                                   <div className="col s12 m4 ">
                                         <div className="info-item flex">
                                            <span className="coin-link-title mr-2" style={{ minWidth: '130px' }}>Market Cap</span>
                                            <span className="badge grey darken-2 white-text" style={{marginLeft:'0'}}>Rank #{market_cap_rank}</span>
                                         </div>
                                        <div className="info-item flex">
                                            <span className="coin-link-title mr-2 " style={{minWidth:'130px'}}>Website</span>
                                            <span className="white-text blue lighten-5">
                                                <a href={homepage[0]} alt={name} target="_blank" rel="noopener noreferrer">{urlString(homepage[0])}</a>
                                            </span>
                                        </div>
                                        <div className="info-item flex">
                                            <span className="coin-link-title mr-2 " style={{ minWidth: '130px' }}>Explorers</span>
                                                {blokchainSites}
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col s10">
                                        <table style={{width:'100%'}}>
                                            <thead>
                                            <tr>
                                                <th>Market Cap </th>
                                                <th>Volume (24h)</th>
                                                <th>Circulating Supply</th>
                                                <th>Total Supply</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>{market_cap[currency]}</td>
                                                    <td>{total_volume[currency]}</td>
                                                    <td>{circulating_supply}</td>
                                                    <td>{total_supply}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ChartFilter />
                <div className="chart-section" style={{width:'100%'}}>
                    {marketChartData && <LineChart chartData={filteredChartData } />  }
                </div>
                
                <div id="description-section" className="col s12 m12">
                    <div className="card">
                 
                        <div className="card-content">
                            <h6 className="card-title">About {name}</h6>
                            <div  dangerouslySetInnerHTML={{ __html: htmlDecode(description.en) }} />
                        </div>
                      
                    </div>
                </div>
            </MaterializeInit>
        )

    }else {
        return <Preloader />
    }
}

CryptoDetail.propTypes = {
    cryptoCurrency:PropTypes.object
}

export default CryptoDetail
