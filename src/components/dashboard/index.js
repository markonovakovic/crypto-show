import React, { useEffect, useContext } from 'react'
import Preloader from '../UI/Preloader';
import AuthContext from '../../context/auth/authContext';
const Dashboard = () => {
    const { currentUser } =  useContext(AuthContext);
    const { displayName, email, phoneNumber } = currentUser;
    useEffect(() =>{
       
    },[]);
  if(currentUser){
    return (
        <div>
            Dashboard
            <h3>{displayName}</h3>
        </div>
    )

  } else {
    return <Preloader />
  }
}

export default Dashboard;
